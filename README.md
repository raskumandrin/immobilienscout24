Dear Stanislav,

I have another crawl job for you 
I want to generate the real estate agents from the following search results below.

Number 1: 612 Real Estate Agents in Munich
http://www.immobilienscout24.de/Anbieter/s/P-1/Bayern/Muenchen/0/3,2,4?reportingDocumentName=%2Fde%2Fanbieter%2Fresult&reporting.worldId=0&sessionCookieRemoval=false&codedCity=TfxuY2hlbg~~&codedRegion=QmF5ZXJu&blid=2&skid=59&geocodeid=1276002059&includeRegions=true

Number 2: 976 Real Estate Agents in Berlin
http://www.immobilienscout24.de/Anbieter/s/P-1/Berlin/Berlin/0/3,2,4?reportingDocumentName=%2Fde%2Fanbieter%2Fresult&reporting.worldId=0&sessionCookieRemoval=false&codedCity=QmVybGlu&codedRegion=QmVybGlu&blid=3&skid=1&geocodeid=1276003001&includeRegions=true

Number 3: 411 Real Estate Agents in Hamburg
http://www.immobilienscout24.de/Anbieter/s/P-1/Hamburg/Hamburg/0/3,2,4?reportingDocumentName=%2Fde%2Fanbieter%2Fresult&reporting.worldId=0&sessionCookieRemoval=false&codedCity=SGFtYnVyZw~~&codedRegion=SGFtYnVyZw~~&blid=6&skid=1&geocodeid=1276006001&includeRegions=true

You need to check the profile page of each of the real estate agents.
Example http://www.immobilienscout24.de/Anbieter/hausmann-immobilien/ac21798e71951dc9fbc?navigationbarurl=%2fAnbieter%2fs%2fP-1%2fHamburg%2fHamburg%2f0%2f3%2c2%2c4%3fpagePrefix%3dall%26includeRegions%3dtrue%26top%3dac21798e71951dc9fbc

On this page we need 
1) next to the picture of the real estate agent below the line "Kontaktieren Sie uns" the contact person. Example
Herr Erwin Dittmeyer freut sich darauf, von Ihnen zu hören.
Herr = Mr. (GENDER)
Erwin (FIRST NAME)
Dittmeyer (LAST NAME). 

2) Über uns (ABOUT US -> in one field)

3) Wir sind spezialisiert auf (WE ARE SPEZIALIZED IN)
data to be generated in separated Excel fields

4) Wir sind in folgenden Regionen für Sie tätig:(WE ARE ACTIVE IN REGIONS)
data to be generated in separated Excel fields

5) Wir bieten Ihnen folgende Dienstleistungen an (SERVICES)
data to be generated in separated Excel fields

6) On each profile page of the real estate agent you also see a link at the top called "Impressum" (=imprint). Example http://www.immobilienscout24.de/Anbieter/branchenbuch/imprint/ac21798e71951dc9fbc

On this page you need to crawl 
- Dittmeyer Immobilien GmbH (COMPANY NAME)
- Jupiterweg 6 (STREET)
- 85586 (ZIP CODE)
- Poing (CITY)
- Tel: 08121 9862944 (PHONE)
- Mobil: 0178 1919975 (MOBILE PHONE)
- Fax: 032 221502962 (FAX)
- E-Mail: dittmeyer.immobilien@gmail.com (E-MAIL)
- Homepage: www.Dittmeyer-Immobilien.de (URL)

As for the previous job at the end all data for each of the real estate agents per city must be put in an Excel file in separated fields for each value.

There is not such a big hurry this time.

Are you interested? If yes, let me know your price.

Best
Dirk

========================

1) three result sets are placed to three sheets of xls-file
first row contains title for columns

2) there are situation where I cannot split name into gender, name and surname. In these cases, i make one more column for such text. It may be name of not two words (three, four - script does not know which part is name, and which is surname). also there are titles of firms

3) I splited specializations with « | »

4) for 10 values from previous message i made different columns. after them, there is a column with other specializations. after it there is a column with all specializations

5) there is short description and long description (point 2 of your task). i put them both in different columns.

6) i didn't find Homepage values anywhere. please, send me links (it may be one page, to find out what rule i need to add to scraper) of the firm with this value.

7) as last time, I added a link (last column) to firm page, so you (and me) could check data.


of course, i am ready to change data presentation, if you will suggest more convenient one for you. and if there are errors, i am ready to fix them (i debugged data, but there may be left some bugs)

=========================

Re 2) "I made one more column" = value in column D, right?
It seems you have not included the company name in your scrape job?
Could you add this as well please.

Columns A, B, C and the company name field should be always filled.

Re 6) You are right, Strangewise, there is no website address available.
You could extract the url from the email address though and put it in a separate column, ok?

==========================

> "I made one more column" = value in column D, right?

yes.

> It seems you have not included the company name in your scrape job?
> Could you add this as well please.

Yes, I scraped it, but forgot to put to result data. in the attached new file this information exists

> Columns A, B, C and the company name field should be always filled.

what should i put in these columns for example for this pages:
1) http://www.immobilienscout24.de/Anbieter/rueth-immobilien/a17afd79665f6e94e48a5
2) http://www.immobilienscout24.de/Anbieter/mr-lodge-gmbh/a81158484d7199c1a90
3) http://www.immobilienscout24.de/Anbieter/cityappartment-muenchen/afee023de6ae83cce5f4ae5

> You could extract the url from the email address though and put it in a separate column, ok?

done

