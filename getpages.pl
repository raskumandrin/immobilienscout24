#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;

my $search_url_head = 'http://www.immobilienscout24.de/Anbieter/s/P-';

my %cities = (
	Munich  => '/Bayern/Muenchen/0/3,2,4?reportingDocumentName=%2Fde%2Fanbieter%2Fresult&reporting.worldId=0&sessionCookieRemoval=false&codedCity=TfxuY2hlbg~~&codedRegion=QmF5ZXJu&blid=2&skid=59&geocodeid=1276002059&includeRegions=true',
	Berlin  => '/Berlin/Berlin/0/3,2,4?reportingDocumentName=%2Fde%2Fanbieter%2Fresult&reporting.worldId=0&sessionCookieRemoval=false&codedCity=QmVybGlu&codedRegion=QmVybGlu&blid=3&skid=1&geocodeid=1276003001&includeRegions=true',
	Hamburg => '/Hamburg/Hamburg/0/3,2,4?reportingDocumentName=%2Fde%2Fanbieter%2Fresult&reporting.worldId=0&sessionCookieRemoval=false&codedCity=SGFtYnVyZw~~&codedRegion=SGFtYnVyZw~~&blid=6&skid=1&geocodeid=1276006001&includeRegions=true',
);

my %pages = (
	Munich  => 32,
	Berlin  => 49,
	Hamburg => 21,
);

foreach my $city (keys %cities) {
	`mkdir -p data/$city-profile`;
	`mkdir -p data/$city-print`;
	
	foreach my $page (1..$pages{$city}) {
		my %links;

#		say "$search_url_head$page$cities{$city}";

		Mojo::DOM->new( $ua->get("$search_url_head$page$cities{$city}")->res->body )->find('a')->each(sub {
			my $a = shift;
#			say $a->{href};
			
			if (defined $a->{href} and $a->{href} =~ qr(/Anbieter/) and $a->{href} !~ qr(/s/P-) ) {
				$links{ $a->{href} } = undef;
#				say "\t".$a->{href};
			}
		});
		
		foreach my $url (keys %links) {
#			say "\t=".$url;
			my ($hash) = ( $url =~ /^[^;\?]+\/([^\/]+?)[;\?]/ );
#			say "\t***".$hash;

			$ua->get( "http://www.immobilienscout24.de$url" )->res->content->asset->move_to("data/$city-profile/$hash.html");
			$ua->get( "http://www.immobilienscout24.de/Anbieter/branchenbuch/imprint/$hash" )->res->content->asset->move_to("data/$city-print/$hash.html");
		};
		undef %links;
		
#		exit if $page >=2;
	}
}
__END__

#	$ua->get($states{$state})->res->content->asset->move_to("data/$state.html");

	Mojo::DOM->new( $ua->get($states{$state})->res->body )->find('a')->each(sub {
		my $a = shift;
#		say $a->text;

		if (defined $a->{href} and $a->{href} =~ qr($state\/) and $a->{href} =~ qr(hostels) ) {
#			say $a->{href};

#			/hostels/Australia/Tasmania/Bicheno
#			/hostels/Australia/Tasmania/Bridport
#			/hostels/Australia/Tasmania/Coles-Bay
#			/hostels/Australia/Tasmania/Devonport
#			/hostels/Australia/Tasmania/Eaglehawk-Neck

			my $city_link = $a->{href};
			my ($city) = ( $city_link =~ /\/([^\/]+)$/ );
#			say $city;

			$ua->get( "http://www.hostelz.com$city_link" )->res->content->asset->move_to("data/$state/$city.html");

		}
#		say $a->text;

	});

}