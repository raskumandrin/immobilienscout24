#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::Util qw(slurp trim);


use utf8;

use Encode;

my @cities = qw(Muenchen Berlin Hamburg);

my %specialization_list;
my %operation_areas;
my %other_services;

my $max_specialization_list;
my $max_operation_areas;
my $max_other_services;

my %important_spec = (
	'Wohnungen zum Kauf'    => 1,
	'Häuser zum Kauf'       => 1,
	'Wohnungen zur Miete'   => 1,
	'Anlageimmobilien'      => 1,
	'Häuser zur Miete'      => 1,
	'Gewerbeimmobilien'     => 1,
	'Auslandsimmobilien'    => 1,
	'Seniorenimmobilien'    => 1,
	'Zwangsversteigerungen' => 1,
	'Grundstücke zum Kauf'  => 1,
);

foreach my $city (@cities) {
	opendir(my $dh, "data/$city-profile") || die;
	open (my $f,'>',"$city.csv");


	while(my $file = readdir $dh) {
		next if $file =~ /^\./;

		my %result;

		my $dom = Mojo::DOM->new( slurp("data/$city-profile/$file") );
		
		if ( $dom->at('p#realtorDetails-name strong') ) {
			$result{realtor_details} = $dom->at('p#realtorDetails-name strong')->text;
			
			$result{realtor_details} = decode('Latin-1', $result{realtor_details} );
			
			if ( $result{realtor_details} =~ /^Herr\s+(\w+)\s+(\w+)$/ ) {
				$result{gender} = 'Herr';
				$result{name} = $1;
				$result{surname} = $2;
			}
			if ( $result{realtor_details} =~ /^Frau\s+(\w+)\s+(\w+)$/ ) {
				$result{gender} = 'Frau';
				$result{name} = $1;
				$result{surname} = $2;
			}
			$result{realtor_details} = '' if $result{gender};
		}
		
		$result{realtor_details_long} = $dom->at('p#realtor-directory-detail-long')->text
			if $dom->at('p#realtor-directory-detail-long');
		$result{realtor_details_short} = $dom->at('p#realtor-directory-detail-short')->text
			if $dom->at('p#realtor-directory-detail-short');
	
		my $local_specialization_list;
		my $local_operation_areas;
		my $local_other_services;
	
		# realtor-specialization-list
		for my $e ($dom->find('ul#realtor-specialization-list li')->each) {
			$specialization_list{$e->text} ++;
			$local_specialization_list ++;
		}

		# realtor-operation-areas
		for my $e ($dom->find('ul#realtor-operation-areas li')->each) {
			$operation_areas{$e->text} ++;
			$local_operation_areas ++;
		}
		
		# realtor-other-services
		for my $e ($dom->find('ul#realtor-other-services li')->each) {
			$other_services{$e->text} ++;
			$local_other_services ++;
		}
		
#		$max_specialization_list = $local_specialization_list > $max_specialization_list ? $local_specialization_list : $max_specialization_list;
#		$max_operation_areas = $local_operation_areas > $max_operation_areas ? $local_operation_areas : $max_operation_areas;
#		$max_other_services = $local_other_services > $max_other_services ? $local_other_services : $max_other_services;
		
		undef $dom;
		
		if ($file =~ /;/) {
			my ( $hash ) = ( $file =~ /^([^;]+);/ );
			$file = "$hash.html";
		}
		
		my $dom = Mojo::DOM->new( slurp("data/$city-print/$file") );

		$result{company_name} = $dom->at('h3')->text
			if $dom->at('h3');
		
		my $full_contacts = $dom->find('div.is24-text')->to_string;
				
		$full_contacts =~ s/\s+/ /g;
		$full_contacts = decode('Latin-1', $full_contacts );
		( $result{company_addr} ) = ( $full_contacts =~ /<p>(.+?)<br>/s );
		( $result{company_postindex},$result{company_city} ) = ( $full_contacts =~ /<br>\s*(\d+)\s+(.+?)<\/p>/s );

		$result{company_addr} = trim($result{company_addr}) if $result{company_addr};
		$result{company_postindex} = trim($result{company_postindex}) if $result{company_postindex};
		$result{company_city} = trim($result{company_city}) if $result{company_city};


		( $result{tel} ) = ( $full_contacts =~ /<span>Tel:\s<\/span>(.+?)<br/ );
		( $result{mobil} ) = ( $full_contacts =~ /<span>Mobil:\s<\/span>(.+?)<br/ );
		( $result{fax} ) = ( $full_contacts =~ /<span>Fax:\s<\/span>(.+?)<br/ );
		( $result{email} ) = ( $full_contacts =~ /<span>E-Mail:\s<\/span>(.+?)<br/ );

		print $f "$result{gender}\t$result{name}\t$result{surname}\t$result{realtor_details}";

#		print $f "\t$result{realtor_details_short}\t$result{realtor_details_long}";

		print $f "\t$result{company_addr}\t$result{company_postindex}\t$result{company_city}";
		print $f "\t$result{tel}\t$result{mobil}\t$result{fax}\t$result{email}";


		print $f "\t$file\n";
		
		undef $dom;
	}
	exit

	close $f;
	closedir $dh;
	
}



open (my $f,'>',"list_specialization.csv");
print $f "$specialization_list{$_}\t$_\n" foreach reverse sort { $specialization_list{$a} <=> $specialization_list{$b} } keys %specialization_list;
close $f;

open (my $f,'>',"list_operation_areas.csv");
print $f "$operation_areas{$_}\t$_\n" foreach reverse sort { $operation_areas{$a} <=> $operation_areas{$b} } keys %operation_areas;
close $f;

open (my $f,'>',"list_other_services.csv");
print $f "$other_services{$_}\t$_\n" foreach reverse sort { $other_services{$a} <=> $other_services{$b} } keys %other_services;
close $f;

#open (my $f,'>',"list_count.txt");
#print $f "max specializations: $max_specialization_list\n";
#print $f "max areas: $max_operation_areas\n";
#print $f "max other services: $max_other_services\n";
#close $f;


__END__

foreach my $city (keys %cities) {
	`mkdir -p data/$city-profile`;
	`mkdir -p data/$city-print`;
	
	foreach my $page (1..$pages{$city}) {
		my %links;

#		say "$search_url_head$page$cities{$city}";

		Mojo::DOM->new( $ua->get("$search_url_head$page$cities{$city}")->res->body )->find('a')->each(sub {
			my $a = shift;
#			say $a->{href};
			
			if (defined $a->{href} and $a->{href} =~ qr(/Anbieter/) and $a->{href} !~ qr(/s/P-) ) {
				$links{ $a->{href} } = undef;
#				say "\t".$a->{href};
			}
		});
		
		foreach my $url (keys %links) {
#			say "\t=".$url;
			my ($hash) = ( $url =~ /^[^;\?]+\/([^\/]+?)[;\?]/ );
#			say "\t***".$hash;

			$ua->get( "http://www.immobilienscout24.de$url" )->res->content->asset->move_to("data/$city-profile/$hash.html");
			$ua->get( "http://www.immobilienscout24.de/Anbieter/branchenbuch/imprint/$hash" )->res->content->asset->move_to("data/$city-print/$hash.html");
		};
		undef %links;
		
#		exit if $page >=2;
	}
}
__END__

#	$ua->get($states{$state})->res->content->asset->move_to("data/$state.html");

	Mojo::DOM->new( $ua->get($states{$state})->res->body )->find('a')->each(sub {
		my $a = shift;
#		say $a->text;

		if (defined $a->{href} and $a->{href} =~ qr($state\/) and $a->{href} =~ qr(hostels) ) {
#			say $a->{href};

#			/hostels/Australia/Tasmania/Bicheno
#			/hostels/Australia/Tasmania/Bridport
#			/hostels/Australia/Tasmania/Coles-Bay
#			/hostels/Australia/Tasmania/Devonport
#			/hostels/Australia/Tasmania/Eaglehawk-Neck

			my $city_link = $a->{href};
			my ($city) = ( $city_link =~ /\/([^\/]+)$/ );
#			say $city;

			$ua->get( "http://www.hostelz.com$city_link" )->res->content->asset->move_to("data/$state/$city.html");

		}
#		say $a->text;

	});

}



#!/usr/bin/perl

use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;
use Mojo::Util qw(slurp spurt);

%url;

open(my $fh,'<','list.txt');
while (<$fh>) {
	chomp;
	my ($n,$u) = ( /(\d+): (.*)/ );
	$url{$n} = 'http://www.ish2013.com/'.$u;
}
close $fh;

foreach my $num (1..1056) {
	my %c;

	my $dom = Mojo::DOM->new( slurp("firms/$num.html") );

	$dom->find('div.manufacturer_detail_preview strong')->each(sub {
		$c{name} = shift->text();
	});


	my $addr;
	my $count;
	if ( $dom->find('div.manufacturer_detail_preview div')->size ) {
		my $firm = $dom->find('div.manufacturer_detail_preview div')->to_string;

		unless ($c{name}) {
			( $c{name} ) = ( $firm =~ /(.+)<br/ );
		}


		$firm =~ s/<br\s\/>/ /g;
		($addr) = ( $firm =~ /<\/strong>([^<]+)<img/ );

		my @a = split(/\(Germany\)/,$addr);

		if ($a[0]) {
			( $c{str1},$c{ind1},$c{cit1} ) = ($a[0] =~ /(.*) (\d{5}) (.*)/ );
		}
		if ($a[1]) {
			( $c{str2},$c{ind2},$c{cit2} ) = ($a[1] =~ /(.*) (\d{5}) (.*)/ );

			if ($c{str2} or $c{ind2} or $c{cit2}) {
				$c{c2} = 'Germany';
			}

		}

#		$c{a0} = $a[0];
#		$c{a1} = $a[1];


		( $c{phone} ) = ($firm =~ /icon_tel\.gif[^>]+>([^<]+)<img/);
		( $c{fax} ) = ($firm =~ /faxsym_g\.png[^>]+>([^<]+)<img/);
		( $c{mail} ) = ($firm =~ /icon_mail\.gif[^>]+>(.+)<img/);
		if ( $c{mail} =~ /<\/a>/ ) {
			( $c{mail} ) = ( $c{mail} =~ /">(.+)<\/a>/);
		};
		( $c{url} ) = ($firm =~ /icon_url\.gif[^>]+>(.+)<\/div/);
		if ( $c{url} =~ /<\/a>/ ) {
			( $c{url} ) = ( $c{url} =~ /">(.+)<\/a>/);
		};
	}

#	say "$num: $count" if $count>4;

#	say "$num";

#	say "\t0: $c{str1} ||| $c{ind1} ||| $c{cit1}";
#	say "\t1: $c{str2} ||| $c{ind2} ||| $c{cit2}";
#	say "\tname: $c{name}";
#	say "\taddr: $addr";
#	say "\tphone: $c{phone}";
#	say "\tfax: $c{fax}";
#	say "\tmail: $c{mail}";
#	say "\turl: $c{url}";

foreach my $key (qw(str1 ind1 cit1 str2 ind2 cit2 phone fax mail url)){
	$c{$key} =~ s/\s+$//g;
	$c{$key} =~ s/,$//g;
	$c{$key} =~ s/\s+$//g;
	$c{$key} =~ s/^\s+//g;
	$c{$key} =~ s/^,//g;
	$c{$key} =~ s/^\s+//g;

}


if ($num == 242) {
	$c{name} = 'DWA Landesverband Hessen/Rheinland-Pfalz/Saarland e.V.';
	$c{ind1} = '1520108';
	$c{ind2} = '1520106';
	$c{c2} = 'Germany';
}

if ($num == 875) {
	$c{str1} = 'Bültestr. 70 b';
	$c{ind1} = '32584';
	$c{cit1} = 'Löhne';
}

if ($num == 875) {
	$c{str1} = 'Bültestr. 70 b';
	$c{ind1} = '32584';
	$c{cit1} = 'Löhne';
}

if ($num == 114) {
	$c{mail} = 'sklotz@rubinetteriebresciane.it, amartin@bonomigmbh.de, amartin@bonomigmbh.com';
	$c{url} = 'www.bonomi.it, www.bonomigmbh.com';
}
if ($num == 168) {
	$c{mail} = 'j.thomson@ceramtec.de, mechanical_systems@ceramtec.de';
	$c{url} = 'www.ceramtec.com/ish';
}

if ($num == 466) {
	$c{mail} = 'd.schaefer@industrilas.de, info@industrilas.de';
	$c{url} = 'www.klima-flex.de, www.industrilas.de';
}

if ($num == 534) {
	$c{mail} = 'a.montag@knipex.de, info@knipex.de';
	$c{url} = 'www.knipex.de';
}

if ($num == 598) {
	$c{mail} = 'info@maico.de, iris.hug@maico.de';
	$c{url} = 'www.maico-ventilatoren.com';
}
if ($num == 659) {
	$c{mail} = 'info@neoperl.de, info@neoperl.net';
	$c{url} = 'www.neoperl.net';
}
if ($num == 719) {
	$c{mail} = 'verkauf3@promat.de';
	$c{url} = 'www.promat.de, www.promat.de/twd';
}
if ($num == 782) {
	$c{mail} = 'messe@ruch.de, info@ruch.de';
	$c{url} = 'www.ruch.de';
}
if ($num == 786) {
	$c{mail} = 'maria.krueselmann@ruetgers.com, info@ruetgers.com';
	$c{url} = 'www.ruetgers.com';
}
if ($num == 972) {
	$c{mail} = 'info@vdzev.de';
	$c{url} = 'www.intelligent-heizen.info, www.vdzev.de';
}


print "$c{name}\t$c{str1}\t$c{ind1}\t$c{cit1}\tGermany\t$c{str2}\t$c{ind2}\t$c{cit2}\t$c{c2}\t$c{phone}\t$c{fax}\t$c{mail}\t$c{url}\t$url{$num}\n";
