#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::Util qw(slurp trim);
use Mojo::ByteStream 'b';
use Excel::Writer::XLSX;
use Encode 'decode';
use utf8;

use Encode;
my @cities = qw( Hamburg Muenchen Berlin  );

my %specialization_list;
my %operation_areas;
my %other_services;

my $max_specialization_list;
my $max_operation_areas;
my $max_other_services;

my %important_spec = (
	'Wohnungen zum Kauf'    => 1,
	'Häuser zum Kauf'       => 1,
	'Wohnungen zur Miete'   => 1,
	'Anlageimmobilien'      => 1,
	'Häuser zur Miete'      => 1,
	'Gewerbeimmobilien'     => 1,
	'Auslandsimmobilien'    => 1,
	'Seniorenimmobilien'    => 1,
	'Zwangsversteigerungen' => 1,
	'Grundstücke zum Kauf'  => 1,
);

# Create a new Excel workbook
my $workbook = Excel::Writer::XLSX->new( 'res.xlsx' );
 
#open my $fh, '>', \my $str or die "Failed to open filehandle: $!";
#my $workbook = Excel::Writer::XLSX->new( $fh );

my $format_blank = $workbook->add_format();
	
foreach my $city (@cities) {
	opendir(my $dh, "data/$city-profile") || die;
#	open (my $f,'>',"$city.csv");

	my $worksheet = $workbook->add_worksheet($city);
	
	my $row=0;
	$worksheet->write($row,0,'Gender');
	$worksheet->write($row,0,'Gender');
	$worksheet->write($row,1,'Name');
	$worksheet->write($row,2,'Surname');
	$worksheet->write($row,3,'Real estate agent details');
	$worksheet->write($row,4,'Company name');
	$worksheet->write($row,5,'Street');
	$worksheet->write($row,6,'Postindex');
	$worksheet->write($row,7,'City');
	$worksheet->write($row,8,'Tel');
	$worksheet->write($row,9,'Mobil');
	$worksheet->write($row,10,'Fax');
	$worksheet->write($row,11,'E-mail');
	$worksheet->write($row,12,'Website');
	$worksheet->write($row,13,'Real estate agency details short');
	$worksheet->write($row,14,'Real estate agency details long');
	
	$worksheet->write($row,15,'Wohnungen zum Kauf') ;
	$worksheet->write($row,16,'Häuser zum Kauf');
	$worksheet->write($row,17,'Wohnungen zur Miete') ;
	$worksheet->write($row,18,'Anlageimmobilien') ;
	$worksheet->write($row,19,'Häuser zur Miete') ;
	$worksheet->write($row,20,'Gewerbeimmobilien');
	$worksheet->write($row,21,'Auslandsimmobilien') ;
	$worksheet->write($row,22,'Seniorenimmobilien') ;
	$worksheet->write($row,23,'Zwangsversteigerungen');
	$worksheet->write($row,24,'Grundstücke zum Kauf');
	
	$worksheet->write($row,25, 'specialization-list' );
	$worksheet->write($row,26, 'specialization-list full' );
	$worksheet->write($row,27, 'operation-areas' );
	$worksheet->write($row,28, 'other-services' );

	$worksheet->write($row,29, 'url' );
	
	$worksheet->write($row,30,'Mobil');
	$worksheet->write($row,31,'Telefon');
	$worksheet->write($row,32,'Telefax');
	$worksheet->write($row,33,'Street');
	$worksheet->write($row,34,'Postcode');
	$worksheet->write($row,35,'City');

	$worksheet->write($row,35,'Facebook');
	
	$row++;

	while(my $file = readdir $dh) {
		next if $file =~ /^\./;

		my %result;

		my $dom = Mojo::DOM->new( b(slurp("data/$city-profile/$file"))->encode('Latin-1') );
#		$dom = b($dom)->encode('UTF-8');
		
		if ( $dom->at('p#realtorDetails-name strong') ) {
			$result{realtor_details} = $dom->at('p#realtorDetails-name strong')->text;
			
#			$result{realtor_details} = decode('Latin-1', $result{realtor_details} );
			
			if ( $result{realtor_details} =~ /^Herr\s+(\w+)\s+(\w+)$/ ) {
				$result{gender} = 'Herr';
				$result{name} = $1;
				$result{surname} = $2;
			}
			if ( $result{realtor_details} =~ /^Frau\s+(\w+)\s+(\w+)$/ ) {
				$result{gender} = 'Frau';
				$result{name} = $1;
				$result{surname} = $2;
			}
			$result{realtor_details} = '' if $result{gender};
		}
		
		$result{realtor_details_long} = $dom->at('p#realtor-directory-detail-long')->text
			if $dom->at('p#realtor-directory-detail-long');
		$result{realtor_details_short} = $dom->at('p#realtor-directory-detail-short')->text
			if $dom->at('p#realtor-directory-detail-short');

		$worksheet->write($row,29, $dom->find('link[rel="canonical"]')->attr('href') );

		$result{facebook} = $dom->at('a#facebook-link')->attr('href')
			if $dom->at('a#facebook-link');
#				<a id="facebook-link" href="http://facebook.com/DavidBorckImmobilien" target="_blank" onclick="track(33, 'bb_fb_link')">Zum Facebook-Profil</a>

	
		my $local_specialization_list;
		my $local_operation_areas;
		my $local_other_services;
	
		# realtor-specialization-list
		my @sp;
		my @sp1;
		for my $e ($dom->find('ul#realtor-specialization-list li')->each) {
			$specialization_list{$e->text} ++;
			$local_specialization_list ++;
			push @sp,$e->text;
			
			push @sp1,$e->text if $important_spec{$e->text};
			
			$worksheet->write($row,15,'Wohnungen zum Kauf') if $e->text eq 'Wohnungen zum Kauf';
			$worksheet->write($row,16,'Häuser zum Kauf') if $e->text eq 'Häuser zum Kauf';
			$worksheet->write($row,17,'Wohnungen zur Miete') if $e->text eq 'Wohnungen zur Miete';
			$worksheet->write($row,18,'Anlageimmobilien') if $e->text eq 'Anlageimmobilien';
			$worksheet->write($row,19,'Häuser zur Miete') if $e->text eq 'Häuser zur Miete';
			$worksheet->write($row,20,'Gewerbeimmobilien') if $e->text eq 'Gewerbeimmobilien';
			$worksheet->write($row,21,'Auslandsimmobilien') if $e->text eq 'Auslandsimmobilien';
			$worksheet->write($row,22,'Seniorenimmobilien') if $e->text eq 'Seniorenimmobilien';
			$worksheet->write($row,23,'Zwangsversteigerungen') if $e->text eq 'Zwangsversteigerungen';
			$worksheet->write($row,24,'Grundstücke zum Kauf') if $e->text eq 'Grundstücke zum Kauf';

			
			
		}

		# realtor-operation-areas
		my @ar;
		for my $e ($dom->find('ul#realtor-operation-areas li')->each) {
			$operation_areas{$e->text} ++;
			$local_operation_areas ++;
			push @ar,$e->text;
		}
		
		# realtor-other-services
		my @os;
		for my $e ($dom->find('ul#realtor-other-services li')->each) {
			$other_services{$e->text} ++;
			$local_other_services ++;
			push @os,$e->text;
		}
		
#		$max_specialization_list = $local_specialization_list > $max_specialization_list ? $local_specialization_list : $max_specialization_list;
#		$max_operation_areas = $local_operation_areas > $max_operation_areas ? $local_operation_areas : $max_operation_areas;
#		$max_other_services = $local_other_services > $max_other_services ? $local_other_services : $max_other_services;
		
		
# # # # # # # # # # # # # # # # # # # # # # # # #
#realtorDetails-contactDetails
		
		my $full_contacts2 = $dom->find('div#realtorDetails-contactDetails')->to_string;
		$full_contacts2 =~ s/\s+/ /g;
#		$full_contacts2 = decode('Latin-1', $full_contacts2 );
		
		( $result{mmobil} ) = ( $full_contacts2 =~ /<span>Mobil:<\/span>(.+?)<\/p/ );
		( $result{mtelefon} ) = ( $full_contacts2 =~ /<span>Telefon:<\/span>(.+?)<\/p/ );
		( $result{mtelefax} ) = ( $full_contacts2 =~ /<span>Telefax:<\/span>(.+?)<\/p/ );
		
		$result{mmobil} = trim($result{mmobil}) if $result{mmobil};
		$result{mtelefon} = trim($result{mtelefon}) if $result{mtelefon};
		$result{mtelefax} = trim($result{mtelefax}) if $result{mtelefax};
		
		$result{maddress} = $dom->at('p#realtorDetails-address')->text
			if $dom->at('p#realtorDetails-address');
		$result{maddress} =~ s/\s+/ /g;
		$result{maddress} = trim($result{maddress}) if $result{maddress};
		
		( $result{mstreet},$result{mpindex},$result{mcity} )
			= ( $result{maddress} =~ /^(.*), (\d{5}) (.*)$/ );
		
# # # # # # # # # # # # # # # # # # # # # # # # #
		
		undef $dom;
		
		if ($file =~ /;/) {
			my ( $hash ) = ( $file =~ /^([^;]+);/ );
			$file = "$hash.html";
		}
		
		my $dom = Mojo::DOM->new( b(slurp("data/$city-print/$file") )->encode('Latin-1') );
	#	$dom = b($dom)->encode('UTF-8');

		$result{company_name} = $dom->at('h3')->text
			if $dom->at('h3');
		
		my $full_contacts = $dom->find('div.is24-text')->to_string if $dom->find('div.is24-text')->size;
		$full_contacts =~ s/\s+/ /g;
		
 
#		$full_contacts = decode('Latin-1', $full_contacts );
		( $result{company_addr} ) = ( $full_contacts =~ /<p>(.+?)<br>/s );
		( $result{company_postindex},$result{company_city} ) = ( $full_contacts =~ /<br>\s*(\d+)\s+(.+?)<\/p>/s );

		$result{company_addr} = trim($result{company_addr}) if $result{company_addr};
		$result{company_postindex} = trim($result{company_postindex}) if $result{company_postindex};
		$result{company_city} = trim($result{company_city}) if $result{company_city};

		( $result{tel} ) = ( $full_contacts =~ /<span>Tel:\s<\/span>(.+?)<br/ );
		( $result{mobil} ) = ( $full_contacts =~ /<span>Mobil:\s<\/span>(.+?)<br/ );
		( $result{fax} ) = ( $full_contacts =~ /<span>Fax:\s<\/span>(.+?)<br/ );
		( $result{email} ) = ( $full_contacts =~ /<span>E-Mail:\s<\/span>(.+?)<br/ );
		( $result{domain} ) = ( $result{email} =~ /@(.*)$/ );
#		print $f "$result{gender}\t$result{name}\t$result{surname}\t$result{realtor_details}";

#		print $f "\t$result{realtor_details_short}\t$result{realtor_details_long}";

#		print $f "\t$result{company_addr}\t$result{company_postindex}\t$result{company_city}";
#		print $f "\t$result{tel}\t$result{mobil}\t$result{fax}\t$result{email}";


#		print $f "\t$file\n";

foreach ( keys %result) {
#	utf8::upgrade($result{$_});
$result{$_} = decode('Latin-1', $result{$_} );
$result{$_} = decode('utf8', $result{$_} );
#	print utf8::is_utf8($result{$_})." : $result{$_}\n";
}
#exit;

		$worksheet->write($row,0,$result{gender});
		$worksheet->write($row,1,$result{name});
		$worksheet->write($row,2,$result{surname});
		$worksheet->write($row,3,$result{realtor_details});
		
#		utf8::upgrade($result{company_name});
#		print utf8::is_utf8($result{company_name})."\n";
#		print utf8::is_utf8($result{company_name})."\n";
#		print "$result{company_name}\n";exit;
		
		$worksheet->write($row,4,$result{company_name});
		$worksheet->write($row,5,$result{company_addr});
		$worksheet->write($row,6,$result{company_postindex});
		$worksheet->write($row,7,$result{company_city});
		$worksheet->write($row,8,$result{tel});
		$worksheet->write($row,9,$result{mobil});
		$worksheet->write($row,10,$result{fax});
		$worksheet->write($row,11,$result{email});
		$worksheet->write($row,12,'http://'.$result{domain});
		$worksheet->write($row,13,$result{realtor_details_short});
		$worksheet->write($row,14,$result{realtor_details_long});

		$worksheet->write($row,25, join(' | ',@sp) );
		$worksheet->write($row,26, join(' | ',@sp1) );
		$worksheet->write($row,27, join(' | ',@ar) );
		$worksheet->write($row,28, join(' | ',@os) );
		

		$worksheet->write($row,30,$result{mmobil});
		$worksheet->write($row,31,$result{mtelefon});
		$worksheet->write($row,32,$result{mtelefax});
		$worksheet->write($row,33,$result{mstreet});
		$worksheet->write($row,34,$result{mpindex});
		$worksheet->write($row,35,$result{mcity});

		$worksheet->write($row,36,$result{facebook});
		
		$row++;
		undef $dom;
		
		
#		$workbook->close(); exit;
	}


#	close $f;
	closedir $dh;
	
}

$workbook->close();

print "\a";
